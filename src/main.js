import Vue from 'vue'
import App from './App.vue'
import  router  from './router';
import BootstrapVue from 'bootstrap-vue';
import './custom.scss';
import VueAxios from 'vue-axios'
import Axios from 'axios';
// import Vue from 'vue'
import Vuex from 'vuex'
import  store  from './store'

Vue.use(Vuex)
Vue.use(VueAxios,Axios)
Vue.config.productionTip = false

Vue.use(BootstrapVue);


new Vue({
  router: router,
  store,
  render: h => h(App),
}).$mount('#app')
