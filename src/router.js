import Home from "./components/Home.vue";
import About from "./components/About.vue";
import Vue from 'vue';
import VueRouter from "vue-router";

// login
import LoginForm from "./components/Login.vue";

Vue.use(VueRouter);
const routes = [{
        path: '/',
        component: Home,
        meta: {
            requiredAuth: true
        }
    },
    {
        path: '/about-us',
        component: About
    },
    {
        path: '/login',
        component: LoginForm
    }
];

let router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiredAuth)) {
        if (localStorage.getItem('access_token') == null) {
            next({
                path: '/login',
                params: {
                    nextUrl: to.fullPath
                }
            })
        } else {
            next()
            /* let user = JSON.parse(localStorage.getItem('user'))
            if (to.matched.some(record => record.meta.is_admin)) {
                if (user.is_admin == 1) {
                    next()
                } else {
                    next({
                        name: 'userboard'
                    })
                }
            } else {
                next()
            } */
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (localStorage.getItem('access_token') == null) {
            next()
        } else {
            next({
                path: '/'
            })
        }
    } else {
        next()
    }
})
export default router