import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        access_token:localStorage.getItem('access_token')
    },
    mutations: {
        setToken (state,payload){
            state.access_token =  payload
        },
        removeToken (state){
            state.access_token = null
        }
    },
    getters: {
        isAccessToken: state => {
            return state.access_token != null
        }
    }
})
export default store